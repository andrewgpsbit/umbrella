function GoToKolhoz(xx,yy,zoom,gr){
    myMap.setCenter([xx,yy],zoom);
    group_id=gr;
    RePaintObjects();

};

//добавляем обьект "Зона покрытия Замкнутая ломаная"
function AreaLineAdd(){
    strokeColor='#ff00ff';
    strokeWidth=4;
    //var arr=[[tx1, ty1]];
    var arr=[[]];
    arealine_add(arr,strokeColor,strokeWidth,true);
};


function SaveObjects(){
    //сначала всё удаляем, потом сразуже сохраняем...
    cid=0;
    var ars = [];
    myCollection.each(function(ob) {
        zxcv=ob;
        cr=ob.options.get("tfig");
        //alert(cr);
        if (cr=="Area"){
            cr=ob.geometry.getCoordinates();
            strokeWidth=ob.options.get("strokeWidth");
            strokeColor=ob.options.get("strokeColor");
            ars.push({cid:cid,type:"Area",strokeWidth:strokeWidth,strokeColor:strokeColor,coords:cr});
            //$.post( "controller/server/lanbilling/maps/addobjects.php?blibase="+billing_id+"&group_id="+group_id, { type: "Poly", coords: cr,strokeWidth:strokeWidth,strokeColor:strokeColor});
            //alert("Poly:"+cr);
        };

        cid++;
    });

    var token = $('#token').val();

    console.log(token, ars);

    $.post( "/map/polygon",
            {
                param: JSON.stringify(ars),
                _token: token
            }
        );
};
var myMap;

// Дождёмся загрузки API и готовности DOM.
ymaps.ready(init);
function init () {
    //обьект с коллекциями обьектов карт
    myCollection = new ymaps.GeoObjectCollection();

    // Создание экземпляра карты и его привязка к контейнеру с
    // заданным id ("map").
    //GetArrayKolhoz();

    myMap = new ymaps.Map('map', {
        // При инициализации карты обязательно нужно указать
        // её центр и коэффициент масштабирования.
        center: [startxx, startyy],
        zoom: 10,
        controls: ['smallMapDefaultSet']
    }),
        //обрабатываем событие по щелчку
        myMap.events.add('click', function (e) {

            //alert(needadd);
            if (needadd!='null'){
                var coords = e.get('coords');
                mclickx=coords[0].toPrecision(6); //где щелкнули?
                mclicky=coords[1].toPrecision(6);
                //добавляем линию оптики
                if (needadd=='0'){TitleAdd(mclickx,mclicky);needadd='null';myMap.cursors.push("arrow");};
                if (needadd=='2'){MetkaAdd(mclickx,mclicky);needadd='null';myMap.cursors.push("arrow");};
                if (needadd=='1'){OptikaLineAdd(mclickx,mclicky);needadd='null';myMap.cursors.push("arrow");};
                if (needadd=='3'){VituhaLineAdd(mclickx,mclicky);needadd='null';myMap.cursors.push("arrow");};
                if (needadd=='5'){CircleAdd(mclickx,mclicky);needadd='null';myMap.cursors.push("arrow");};
                //if (needadd=='4'){AreaLineAdd(mclickx,mclicky);needadd='null';myMap.cursors.push("arrow");};

            };


        });
    // Кнопка сохранения карты
    var mySaveButton = new ymaps.control.Button({
        data:    {content: 'Сохранить'},
        options: {selectOnClick: false,size:'large'}
    });
    mySaveButton.events.add('click', function () {
        SaveObjects();
    });
    myMap.controls.add(mySaveButton, {float: 'right'});
    RePaintObjects();



    //GoToKolhoz(55.753559,37.609218,10,2); // Москва
};

function RePaintObjects(){
    //очищаем холст
    myMap.geoObjects.each(function(ob) {
        myMap.geoObjects.remove(ob);
    });
    //очищаем коллекцию
    myCollection.removeAll();
    //читаем jsoin и выводим на экран содержимое
    $.get('/map/polygon',  // сначала получем список
        {},
        function(e) {
            console.log(e);
            obj_for_load=e;   // загружаем JSON в массив
            PlaceObj();
        });
};
function PlaceObj(){
    //перебираем каждый элемент массива, в зависимости от типа элемента - рисуем..
    i=0;
    for (i in obj_for_load) {
        tp=obj_for_load[i]["type"];


        //if (tp=="Area"){
            txtycoor=obj_for_load[i]["coords"];
            strokeColor=obj_for_load[i]["strokeColor"];
            strokeWidth=obj_for_load[i]["strokeWidth"];
            arealine_add(txtycoor,strokeColor,strokeWidth,false);
        //};


        //myPolyline.editor.startEditing();
    };
};


//Зона покрытия
function arealine_add(txtycoor,strokeColor,strokeWidth,modeadd){
    //alert(txtycoor);
    //alert("polyadd"+tx1+"!"+tx2+"!"+ty1+"!"+ty2+"!"+strokeColor+"!"+strokeWidth);
    //var arr=[[tx1, ty1],[tx2, ty2]];
    var myPolyline = new ymaps.Polygon(txtycoor, {}, {
        // Задаем опции геообъекта.
        // Цвет с прозрачностью.
        strokeColor: strokeColor,
        // Ширину линии.
        strokeWidth: strokeWidth,
        // Максимально допустимое количество вершин в ломаной.
        editorMaxPoints: 50,
        editorMenuManager: function (items) {
            items.push({
                title: "Удалить линию",
                onClick: function () {
                    myCollection.remove(cured);
                    myMap.geoObjects.remove(cured);
                }
            });
            return items;
        },
        draggable: true,
        tfig:"Area",
        // Добавляем в контекстное меню новый пункт, позволяющий удалить ломаную.
    });
    myPolyline.events.add('click', function (e) {
        if (cured!='null') cured.editor.stopEditing();
        e.get('target').editor.startEditing();
        cured=e.get('target');
    });
    myCollection.add(myPolyline);
    myMap.geoObjects.add(myCollection);
    if (modeadd==true){
        //alert("!");
        //включаем сразу режим редактирования!
        myPolyline.editor.startDrawing();
    };
};


