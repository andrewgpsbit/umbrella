<?php

namespace App\Services;

use Illuminate\Http\Response;
use DB;
use App\Constants\Logger;
use Psr\Log\LoggerInterface;

class RedirectTo
{

    /** @var LoggerInterface $logger */
    private $logger;

    public function __construct()
    {

        $this->logger = $logger = app(logger::REDIRECT);

    }

    public function redirect($short)
    {

        if(empty($short)){

            $this->logger->error(
                "Bad request",
                [
                    'short' => $short
                ]
            );

            return response('Bad request', Response::HTTP_BAD_REQUEST);
        }

        $query = DB::table('redirects')
            ->where('short', $short);
        $original = $query->pluck('original')->first();


        if(empty($original)){

            $this->logger->error(
                "Not found",
                [
                    'short' => $short
                ]
            );
            return response('Not found', Response::HTTP_NOT_FOUND);
        }

        $query->increment('count');
        $count = $query->pluck('count')->first();


        $this->logger->info(
            "Success",
            [
                'original' => $original,
                'count' => $count
            ]
        );
        return redirect($original);
    }
}
