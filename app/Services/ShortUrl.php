<?php

namespace App\Services;

use Illuminate\Http\Request;
use GuzzleHttp\Client as Guzzle;
use Exception;
use Illuminate\Http\Response;
use App\Redirect;
use DB;
use App\Constants\Logger;
use Psr\Log\LoggerInterface;

class ShortUrl
{

    const PANEL_BASIC = 'Can put valid url bellow';
    const PANEL_INVALID_URL = 'This url not valid! Put valid url';
    const PANEL_GONE_URL = 'This short url gone! Put other short url';
    const PANEL_SUCCESS_URL = 'Created short url successfully';


    private $short;

    /** @var LoggerInterface $logger */
    private $logger;

    public function __construct()
    {

        $this->logger = $logger = app(logger::SET_SHORT);

    }

    public function set(Request $request)
    {

        $base = request()->getSchemeAndHttpHost().'/';
        $desiredShort = $request->input('desiredShort');
        $originalUrl = $request->input('originalUrl');

        $this->short = (!empty($desiredShort)) ? $desiredShort : $this->randomString();

        $find = Redirect::where('short', $this->short)->first();
        if($find){

            if(!empty($desiredShort)){
                return response(['message' => self::PANEL_GONE_URL], Response::HTTP_GONE);
            }
            $this->short = $this->randomString();
        }

        $config = [
            'base_uri' => $originalUrl,
            'timeout' => 15,
        ];

        $guzzle = new Guzzle($config);

        try{

            $response = $guzzle->request('GET');

            if($response->getStatusCode() != Response::HTTP_OK){

                $this->logger->error(
                    "Check original url",
                    [
                        'code' => $response->getStatusCode(),
                        'msg' => $response->getBody()->getContents(),
                        'short' => $this->short,
                        'desiredShort' => $desiredShort,
                        'original' => $originalUrl
                    ]
                );
                return response(['message' => self::PANEL_INVALID_URL], Response::HTTP_BAD_REQUEST);
            }


            $redirect = Redirect::create([
                'base' => $base,
                'short' => $this->short,
                'original' => $originalUrl,
            ]);

            $this->logger->info(
                "Set short",
                [
                    'redirect_id' => $redirect->id,
                    'base' => $base,
                    'short' => $this->short,
                    'original' => $originalUrl
                ]
            );

            $message = self::PANEL_SUCCESS_URL . ': ' . $base . $redirect->short;

            return response(['message' => $message, 'short' => $redirect->short], Response::HTTP_OK);

        }catch(Exception $e){

            $this->logger->error(
                "Set short",
                [
                    'base' => $base,
                    'short' => $this->short,
                    'original' => $originalUrl,
                    'code' => $e->getCode(),
                    'msg' => $e->getMessage()

                ]
            );
            return response(['message' => self::PANEL_INVALID_URL, 'detail' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    private function randomString($length = 8)
    {
        $str = "";
        $characters = array_merge(range('A','Z'), range('a','z'));//, range('0','9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }
}
