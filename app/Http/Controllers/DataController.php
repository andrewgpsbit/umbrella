<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;;
use App\Services\ShortUrl;
use App\Services\RedirectTo;

class DataController extends Controller
{


    public function setShort(Request $request)
    {

        $shortUrl = new ShortUrl();

        return $shortUrl->set($request);
    }


    public function redirect(Request $request, $short)
    {

        $redirectTo = new RedirectTo();

        return $redirectTo->redirect($short) ;
    }


}
