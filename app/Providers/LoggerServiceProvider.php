<?php

namespace App\Providers;

use App\Constants\Logger as LoggerConstants;
use Illuminate\Log\Writer;
use Illuminate\Support\ServiceProvider;
use Monolog\Logger;

/**
 * Provider for logger setting.
 */
class LoggerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $loggers = [
            LoggerConstants::SET_SHORT => 'set_short',
            LoggerConstants::REDIRECT => 'redirect'
        ];

        foreach ($loggers as $key => $name) {
            $this->app->singleton($key, function () use ($name) {
                $file = storage_path("logs/{$name}.log");

                $logger = new Logger($name);

                $writer = new Writer($logger);
                $writer->useDailyFiles($file);

                return $writer;
            });
        }
    }
}
