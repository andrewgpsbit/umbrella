<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Redirect;


class RedirectDeleter extends Command
{

    protected $signature = 'redirect:deleter';

    public function handle()
    {

        $time = \Carbon\Carbon::now()->subDay(14);

        Redirect::where('created_at', '<', $time)
            ->delete();
    }
}