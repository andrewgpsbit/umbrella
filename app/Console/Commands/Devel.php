<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Constants\logger;


class Devel extends Command
{

    protected $signature = 'dev {id?}';
    protected $description = 'For test development';

    public function handle()
    {
        $logger = app(logger::SAVE);


        dd($this->randomString());

//        $logger->info(
//            'Test devel',
//            [
//                'some_param' => 'some_value',
//            ]
//        );

    }

    function randomString($length = 8) {
        $str = "";
        $characters = array_merge(range('A','Z'), range('a','z'));//, range('0','9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }
}