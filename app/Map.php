<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Map extends Model
{

    public $fillable = [
        'strokeWidth',
        'strokeColor',
        'coords'
    ];

    public $casts = [
        'coords' => 'array'
    ];

}
