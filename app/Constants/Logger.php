<?php

namespace App\Constants;

/**
 * Constants for logger determining.
 */
final class Logger
{
    const SET_SHORT = 'logger:set_short';
    const REDIRECT = 'logger:redirect';

}
