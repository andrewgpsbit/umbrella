(function (){
    "use strict";

    angular.module('app.controllers').controller('AboutCtrl', function ($scope, $http){

        var vm = $scope;

        const base = window.location.origin+'/';

        vm.base = base;

    });

})();