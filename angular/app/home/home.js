(function (){
    "use strict";

    angular.module('app.controllers').controller('HomeCtrl', function ($scope, $http){

        var vm = $scope;

        const base = window.location.origin+'/';
        const panelBasic = 'Can put valid url bellow';

        vm.gen = gen;
        vm.panel = panelBasic;
        vm.base = base;

        function gen() {

            $http.put('short', {desiredShort: vm.desiredShort, originalUrl: vm.originalUrl}).then(successFn, errorFn);

            function successFn(response) {

                vm.panel = response.data.message;
                vm.desiredShort = response.data.short;
            }

            function errorFn(response) {

                if(response.status == 410){
                    return vm.panel = response.data.message;
                }
                vm.panel = response.data.message;
            }

        }
    });

})();