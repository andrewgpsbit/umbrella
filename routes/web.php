<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/api/v1/short', 'DataController@setShort');
Route::put('short', 'DataController@setShort');
Route::get('/map', 'CommonController@index');
Route::get('/map/polygon', 'CommonController@getMap');
Route::post('/map/polygon', 'CommonController@setMap');
Route::get('/{short}', 'DataController@redirect');

